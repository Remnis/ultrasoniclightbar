#include <Adafruit_NeoPixel.h>
#include <NewPing.h>

// Neopixel Config
#define NeoPIN 2
#define NUM_LEDS 30
int brightness = 140;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);

// Sonar
#define SONAR_NUM 5      // Number of sensors.
#define MAX_DISTANCE 300 // Maximum distance (in cm) to ping.

int ledPerSonic = NUM_LEDS / SONAR_NUM;

NewPing sonar[SONAR_NUM] = {   // Sensor object array.
  NewPing(11, 12, MAX_DISTANCE), // Each sensor's trigger pin, echo pin, and max distance to ping. 
  NewPing(9, 10, MAX_DISTANCE), 
  NewPing(7, 8, MAX_DISTANCE),
  NewPing(5, 6, MAX_DISTANCE),
  NewPing(3, 4, MAX_DISTANCE)
};

void setup() {
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
  strip.setBrightness(brightness);
  strip.begin();
  strip.show(); 
  stripTest();
}


void stripTest(){
  for(int i = 0; i < NUM_LEDS; i++){
    strip.setPixelColor(i, strip.Color( 254, 254, 254 ) );
    strip.show();
    delay(40);
  }
  delay(100);
  for(int i = 0; i < NUM_LEDS; i++){
    strip.setPixelColor(i, strip.Color( 0, 0, 0 ) );
    strip.show();
    delay(40);
  }
}
void loop() { 
  for (uint8_t i = 0; i < SONAR_NUM; i++) { // Loop through each sensor and display results.
    delay(50); // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
    Serial.print(i);
    Serial.print("=");
    
    int cm = sonar[i].ping_cm();
    int startLed = i * ledPerSonic;
    Serial.print(cm);
    Serial.print("cm ");

    if(cm > 0){ // prevents false results
      setNeos(i, cm);
    }
  }
  Serial.println();
}

void setNeopixels(int t, int r, int g, int b){
  int startLed = (t * ledPerSonic);
  for(int i = startLed; i < (startLed + ledPerSonic); i++){
    strip.setPixelColor(i, strip.Color( g, r, b ) );
      strip.show();
  }
}


void setNeos(int t, int cm){
  
  int startLed = (t * ledPerSonic);
  int r = map(cm, 5, 300, 254, 20); // color red
  int b = map(cm, 1, 300, 30, 250); // color blue
  int g = map(cm, 1, 300, 0, 254); // color green
  
  for(int i = startLed; i < (startLed + ledPerSonic); i++){
     strip.setPixelColor(i, strip.Color( g, r, b ) );
     strip.show();
  }
}
